# PostgreSQL S3 Backup

Backup PostgreSQL database to an S3 repository

## Dependencies
-   `pg_dump` (sometimes in content of `postgresql-client`, sometimes in `postgresql`. Check your *nix distro)
-   Minio Client (`mc`/`mcli`/`minio-mc`)
-   `uuidgen`
-   \[Development environment\] `jq` for automated testing

## For downstream mantainers
For installation in *nix systems, I recommend the following:
-   \[CONFIG\] Copy the configuration file from `etc/pgs3bak.conf` to `/etc/pgs3bak/pgs3bak.conf`
-   \[EXEC\] Copy the bin file `bin/pgs3bak` to `/usr/bin/pgs3bak` (or `/usr/local/bin/pgs3bak`) and set permissions to be executable
-   \[LIB\] Copy all files inside `lib/` directory to `/usr/lib/pgs3bak/`. _These files don't need to have permissions to be executable_
-   If it is a Linux distro with Systemd support, copy the contents of `systemd/` directory to `/usr/lib/systemd/system/` (or the specific directory where your distro stores the Systemd units provided by packages)

**_IMPORTANT_**
The main executable file (`bin/pgs3bak`) will look for the lib files in that directory by default.
To change this behavior, you can change the location of the lib directory in the configuration file (`etc/pgs3bak.conf`) using the configuration variable `PGS3BAK_LIB_PATH`.
If you do so, remember to patch it before redistributing.

## ENVIRONMENT VARIABLES
These variables can be found in `/etc/pgs3bak/*.conf` files and can be adjusted to your environment.
-   **DUMP VARIABLES**
    -   PGS3BAK_DATABASE_NAME: database name to backup (overriden by `-d <dbname>`)
    -   PGS3BAK_OUTPUT_FILE: path for the output dump (overriden by `-f <file>`)
    -   PGS3BAK_OUTPUT_FORMAT: Output format of the dump (overriden by `-F <format>`)
    -   PGS3BAK_OUTPUT_COMPRESSION: Output compression level of the output file (overriden by `-z <level>`)
    -   PGS3BAK_NO_OWNER: Do not set database owner during dumping (overriden by `-O`)
    -   PGS3BAK_JOBS: Number of parallel jobs for dumping (overriden by `-j <jobs>`)
-   **MINIO/S3 VARIABLES**
    -   PGS3BAK_MINIO_CLIENT: path to executable MinIO Client (default: `mc`). In ArchLinux, use `mcli` (previously exposed as `minio-mc` from AUR)
    -   PGS3BAK_MINIO_ALIAS: name of the MinIO configuration alias to be used (it will inherit credentials to access). If not set, access credentials will be required and an ephemeral config alias will be created (`-a <alias>`)
    -   PGS3BAK_MINIO_URL: S3 (or S3-compliant) instance address (ex: `https://fra1.digitaloceanspaces.com`) (`-u <url>`)
    -   PGS3BAK_MINIO_ACCESS_KEY: Access Key of the instance (`-k <access key>`)
    -   PGS3BAK_MINIO_SECRET_KEY: Secret Key of the instance (`-s <secret key>`)
    -   PGS3BAK_MINIO_BUCKET: Bucket to store object (`-b <bucket>`)
    -   PGS3BAK_MINIO_CREATE_BUCKET_IF_NOT_EXISTS: Create bucket if not exists (default `0`)
    -   PGS3BAK_MINIO_OBJECT_NAME: Object name to store file (`-o <object name>`) (if unset, uses last section of the `-f <file>`|`$PGS3BAK_OUTPUT_FILE`)
-   **MAINTENANCE VARIABLES**
    -   PGS3BAK_ROTATE_OLDER_THAN: Remove older backup dumps if older than this. Format is `14d12h40m` (14 days, 12 hours and 40 minutes)
    -   PGS3BAK_KEEP_LOCAL_DUMP: Do not remove local dump file
-   **INTERNAL VARIABLES**
    -   PGS3BAK_LIB_PATH: Path to internal library files
-   UTILITY ENVIRONMENT VARIABLES FROM POSTGRESQL (if set, they are respected)
    -   PGHOST
    -   PGPORT
    -   PGDATABASE
    -   PGUSER
    -   PGPASSWORD
    -   PGPASSFILE
    -   PGOPTIONS

## Systemd units
We provide Systemd templated service and timer units, you can create a unit instance for each database.
You are able to start/enable a single backup execution using the service or a periodic every 3h (starting at 00:00) using the timer.
The database name has to have the same of the instance name
```shell
# to start pgs3bak to backup database "foobar" periodically every 3 hours
systemctl start pgs3bak@foobar.timer

# to start pgs3bak to execute single backup of database "hello"
systemctl start pgs3bak@hello.service
```

For handling multiple databases, a specific configuration can be created in `/etc/pgs3bak/` directory with the unit instance name.
The Systemd service will load the global configuration from `/etc/pgs3bak/pgs3bak.conf`, and then will overwrite the with `/etc/pgs3bak/<instance_name>.conf`
So if the instance name is started with `systemctl start pgs3bak@foo.service`, it will look for `/etc/pgs3bak/foo.conf`
