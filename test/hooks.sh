#!/usr/bin/env sh

minio_client=$PGS3BAK_MINIO_CLIENT
[ -z "$minio_client" ] && minio_client=mc

setup() {
    count=$1
    count=$((count - 1))
    psql -c "CREATE TABLE fixtures (id BIGSERIAL PRIMARY KEY, value DOUBLE PRECISION DEFAULT random() NOT NULL)"
    values="(DEFAULT, DEFAULT)"
    for _i in $(seq $count); do
        values="${values}, (DEFAULT, DEFAULT)"
    done
    psql -c "INSERT INTO fixtures (id, value) VALUES $values"
    $minio_client alias set fixture "$PGS3BAK_MINIO_URL" "$PGS3BAK_MINIO_ACCESS_KEY" "$PGS3BAK_MINIO_SECRET_KEY" --quiet > /dev/null
    $minio_client mb "fixture/$PGS3BAK_MINIO_BUCKET" --quiet > /dev/null
    $minio_client alias remove fixture --quiet > /dev/null
}

teardown() {
    $minio_client alias set fixture "$PGS3BAK_MINIO_URL" "$PGS3BAK_MINIO_ACCESS_KEY" "$PGS3BAK_MINIO_SECRET_KEY" --quiet > /dev/null
    $minio_client rb "fixture/$PGS3BAK_MINIO_BUCKET" --force --dangerous --quiet > /dev/null
    $minio_client alias remove fixture --quiet > /dev/null
    psql -c "DROP TABLE fixtures"
}
