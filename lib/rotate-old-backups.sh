#!/usr/bin/env sh

## Required
# -a <alias>            Use this named alias
# -b <bucket name>      Rotate files in this bucket
# -t <threshold age>    Minimum age to remove files

while getopts ":a:b:t:" option; do
    case $option in
        a)
            alias=$OPTARG >&2
            ;;
        b)
            bucket_name=$OPTARG >&2
            ;;
        t)
            threshold_age=$OPTARG >&2
            ;;
        \?)
            echo "invalid option -$OPTARG"
            exit 1
            ;;
    esac
done


if [ -z "$alias" ]; then
    echo "Missing alias"
    exit 127
fi
if [ -z "$bucket_name" ]; then
    echo "Missing bucket name"
    exit 127
fi

minio_client=$PGS3BAK_MINIO_CLIENT
[ -z "$minio_client" ] && minio_client=mc

$minio_client find "${alias}/$bucket_name" --older-than "$threshold_age" --exec "$minio_client rm {} ${alias}/$bucket_name"
