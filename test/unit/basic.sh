#!/usr/bin/env sh

minio_client=$PGS3BAK_MINIO_CLIENT
[ -z "$minio_client" ] && minio_client=mc

root_dir=$(dirname "$(realpath "$0")")/../..

## do backup
"$root_dir/bin/pgs3bak" -f /tmp -F plain -z 7 -O -W "$POSTGRES_PASSWORD"

## assert ephemeral alias was removed
aliases=$($minio_client alias list --quiet --json | jq -r '.alias' | grep pgs3bak)
if [ -n "$aliases" ]; then
    echo "ERROR: aliases should have been removed"
    exit 1
fi
$minio_client alias set cloud-storage "$PGS3BAK_MINIO_URL" "$PGS3BAK_MINIO_ACCESS_KEY" "$PGS3BAK_MINIO_SECRET_KEY" --quiet > /dev/null

objects=$($minio_client ls "cloud-storage/$PGS3BAK_MINIO_BUCKET" --quiet --json | jq -cs '. | map(.key)')
if [ "$(echo "$objects" | jq '. | length')" -gt 1 ]; then
    echo "ERROR: more than 1 file was uploaded"
    exit 1
fi

object=$(echo "$objects" | jq -r '.[0]')
$minio_client cp "cloud-storage/$PGS3BAK_MINIO_BUCKET/$object" "/tmp/$object" --quiet

## @TODO pg_restore into another database
rm "/tmp/$object"

$minio_client alias remove cloud-storage --quiet > /dev/null
